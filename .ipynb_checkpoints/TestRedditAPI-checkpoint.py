import praw
from praw.models import MoreComments

import pandas as pd
import numpy as np

import time
import warnings
warnings.filterwarnings("ignore")

import telebot
from telebot import apihelper

import logging
import datetime

# Proxy for Telegram Bot

apihelper.proxy = {'http':'http://user42340:d2faz1@45.15.255.85:5756'}

# Token for Telegram Bot

token_bot = '1281059889:AAFIIYU9VqMprLohp6OAZPuRHCnLVbbBpqQ'

# Initialize Bot

bot = telebot.TeleBot(token_bot)

bot.send_message(-545966419, 'Bot started')

reddit = praw.Reddit(
    user_agent = "testscript by u/fakebot3",              # здесь можно написать любое значение
    client_id = "Af5RvOUDcPhdCQ",                         # выдается при создании приложения на сайте Reddit
    client_secret = "WdCPMDWOejVAYf8VAtjWVVwKZKs-Wg",     # выдается при создании приложения на сайте Reddit
    username = "Various_Top_737",                         # username из личного кабинета
    password = "Lexus28101993"                            # пароль для входа на сайт
)

subreddits = ['wallstreetbets',
              'pennystocks',
              'SatoshiStreetBets']

submission_limit = 1000

current_time = int(time.time())

for subred in subreddits:

    submition_list = [[current_time, submission.id, submission.url, submission.title, submission.num_comments, 
                       submission.author, submission.score, submission.created, submission.selftext, submission.subreddit.id] 
                      for submission in reddit.subreddit(subred).hot(limit = submission_limit)]

    posts_df_columns = ['current_time', 'id', 'link', 'theme', 'comments',             
                        'author_name', 'likes','created', 'content', 'subreddit_id']

    current_post_df = pd.DataFrame(data = submition_list, columns = posts_df_columns)

    file_name = subred + "_" + str(current_time) + ".csv"
    
    current_post_df.to_csv("/root/my-notebooks/reddit/data-submitions/" + file_name)
    
    bot.send_message(-545966419, subred + " - Parser Records: " + str(len(current_post_df.index)))


working_time = int(time.time()) - current_time
working_time = str(datetime.timedelta(seconds = working_time))

bot.send_message(-545966419, 'Bot finished. Working time: ' + working_time)