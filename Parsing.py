#!/usr/bin/env python
# coding: utf-8

# ### Загрузка библиотек

# In[82]:


import praw
from praw.models import MoreComments

import pandas as pd
import numpy as np

import time
import warnings
warnings.filterwarnings("ignore")

from sqlalchemy import create_engine
from IPython.display import display, HTML

import telebot
from telebot import apihelper

import logging
import datetime


# In[83]:


# Proxy for Telegram Bot

apihelper.proxy = {'http':'http://user42340:d2faz1@45.15.255.85:5756'}

# Token for Telegram Bot

token_bot = '1281059889:AAFIIYU9VqMprLohp6OAZPuRHCnLVbbBpqQ'

# Initialize Bot

bot = telebot.TeleBot(token_bot)

bot.send_message(-545966419, 'Bot started')


# ### Подключение к БД

# In[84]:


# Задаем название БД

database = 'reddit'


# In[85]:


# Поключаемся к базе данных

conn = create_engine('mysql+pymysql://myuser:pJHsjfh1@localhost/' + database)


# ### Подключение к Reddit API

# In[86]:


reddit = praw.Reddit(
    user_agent = "testscript by u/fakebot3",              # здесь можно написать любое значение
    client_id = "Af5RvOUDcPhdCQ",                         # выдается при создании приложения на сайте Reddit
    client_secret = "WdCPMDWOejVAYf8VAtjWVVwKZKs-Wg",     # выдается при создании приложения на сайте Reddit
    username = "Various_Top_737",                         # username из личного кабинета
    password = "Lexus28101993"                            # пароль для входа на сайт
)


# ### Переменные

# #### Поля для таблицы с комментариями

# In[87]:


comments_df_columns = ['current_time',       # время сбора комментариев
                       'id',                 # id комментария
                       'author_name',        # имя автора
                       'content',            # текст комментария
                       'created',            # время создания комментария в UnixTime
                       'score',              # количество upvotes у комментария
                       'is_submitter',       # является ли комментатор создателем поста
                       'submission_id',      # id поста
                       'subreddit_id']       # id subreddit


# #### Поля для таблицы с постами

# In[88]:


posts_df_columns = ['current_time',          # время сбора постов
                    'id',                    # id поста
                    'link',                  # ссылка на пост
                    'theme',                 # тема поста
                    'comments',              # количество комметариев
                    'author_name',           # имя автора
                    'likes',                 # количество upvotes у поста
                    'created',               # время создания поста
                    'content',               # текст поста
                    'subreddit_id']          # id subreddit


# #### Другие переменные

# In[89]:


# Названия страниц, которые хотим парсить. Если страниц несколько используем + для конкатенации\

subreddits = ['wallstreetbets',
              'pennystocks',
              'SatoshiStreetBets']

# Количество постов, которое будем парсить

submission_limit = 100

# Глубина комментариев

comments_depth = 5


# ### Создание структуры. Раскомментировать только при первом запуске

# In[90]:


"""comments_info_template_df = pd.DataFrame([['gn19o05',
                                           'sdevil713',
                                           'hello',
                                           1613113269,
                                           True,
                                           'li5sts',
                                           '2th52']], columns = [i for i in comments_df_columns 
                                                                 if i not in ['current_time','score']])


comments_info_template_df.to_sql('comments_info',
                                 conn,
                                 index = False,
                                 if_exists = 'append')


posts_info_template_df = pd.DataFrame([['gn19o05',
                                        'hello',
                                        'hello',
                                        'aa',
                                        1613113269,
                                        'hello',
                                        '2th52']], columns = [i for i in posts_df_columns 
                                                              if i not in ['current_time','comments','likes']])


posts_info_template_df.to_sql('posts_info',
                              conn,
                              index = False,
                              if_exists = 'append')

time_posts_and_comments = pd.DataFrame([[2,2,2]], columns = ['current_time', 'post_time', 'comment_time'])

time_posts_and_comments.to_sql('time_posts_and_comments',
                               conn,
                               index = False,
                               if_exists = 'append')"""


# ### Загрузка данных из таблиц

# #### Загрузка данных о времени постов и комментариев

# In[91]:


time_posts_and_comments = pd.read_sql("""
                                         SELECT
                                           *
                                         FROM
                                           time_posts_and_comments
                                         ORDER BY
                                           post_time DESC
                                         LIMIT 1
                                      """,
                                      conn)


# In[92]:


# Минимальная дата постов

before_posts_time = time_posts_and_comments.loc[0]['post_time']

# Минимальная дата комментариев

before_comments_time = time_posts_and_comments.loc[0]['comment_time']


# ### Функции

# #### Функция для сбора данных о постах и комментариях

# In[93]:


# Текущее время

current_time = int(time.time())

def get_data(subreddits, submission_limit, comments_depth):
   
   # Параметры:
   
   # subreddits - названия страниц, которые хотим парсить. Если страниц несколько используем + для конкатенации
   # submission_limit - количество постов, которое будем парсить
   # comments_depth - глубина комментариев
   

   # Инициализируем DataFrames с постами и комментариями
   
   posts_df = pd.DataFrame(columns = posts_df_columns)
   comments_df = pd.DataFrame(columns = comments_df_columns)

   # Проходим по каждому посту
   
   for submission in reddit.subreddit(subreddits).hot(limit = submission_limit):
   
       # Создаем DataFrame с признаками текущего поста

       current_post_df = pd.DataFrame(data = [[current_time,
                                               submission.id,
                                               submission.url,
                                               submission.title,
                                               submission.num_comments, 
                                               submission.author.name,
                                               submission.score,
                                               submission.created, 
                                               submission.selftext,
                                               submission.subreddit.id]], columns = posts_df_columns)
       
       # Конкатенируем с DataFrame со всеми постами
       
       posts_df = pd.concat([posts_df, current_post_df], ignore_index = True)

       #display(posts_df)
       
       # Делаем запросы на ответы к комментариям

       submission.comments.replace_more(limit = comments_depth)

       # Создаем массив признаков комментариев
       
       comments_list = [[current_time,
                        comment.id,
                        comment.author,
                        comment.body,
                        comment.created_utc,
                        comment.score,
                        comment.is_submitter,
                        submission.id,
                        submission.subreddit.id] for comment in submission.comments.list()]

       # Создаем DataFrame на основе comments_list
       
       current_comment_df = pd.DataFrame(data = comments_list, 
                                         columns = comments_df_columns)

       # Конкатенируем с DataFrame со всеми комментариями
       
       comments_df = pd.concat([comments_df, current_comment_df], ignore_index = True)
       
   return posts_df, comments_df


# #### Функция для записи данных о постах в таблицы

# In[94]:


def save_posts(posts_df):
    
    # Создаем DataFrame с признаками постов, которые меняются со временем
    
    posts_features_df = posts_df[['current_time',
                                  'id',
                                  'subreddit_id',
                                  'comments',
                                  'likes']]

    # Записываем признаки постов, которые меняются со временем в таблицу
    
    posts_features_df.to_sql('posts_features',
                             conn,
                             index = False,
                             if_exists = 'append')
    
    # Создаем DataFrame с полями, которые не меняются со временем
    
    posts_info_df = posts_df.drop(['current_time','comments', 'likes'],
                                  axis = 1)
    
    # Оставляем только те записи, которых еще нет в таблице
    
    posts_new_df = posts_info_df[posts_info_df['created'] > before_posts_time]
    
    current_posts_time = before_posts_time.copy()
    
    if(not posts_new_df.empty):
        current_posts_time = posts_new_df['created'].max()
    
    
        # Записываем в таблицу информацию о новых постах
    
        posts_new_df.to_sql('posts_info',
                             conn,
                             index = False,
                             if_exists = 'append')

    return current_posts_time


# #### Функция для записи данных о комметариях в таблицы

# In[95]:


def save_comments(comments_df):
    
    # Удаляем комментарии, у которых нет автора
    
    comments_df = comments_df[comments_df['author_name'].isna() == False]
    
    # Создаем DataFrame с признаками комментариев, которые меняются со временем 
    
    comments_features_df = comments_df[['current_time',
                                        'id',
                                        'submission_id',
                                        'subreddit_id',
                                        'score']]

    # Записываем признаки комментариев, которые меняются со временем в таблицу
    
    comments_features_df.to_sql('comments_features',
                                conn,
                                index = False,
                                if_exists = 'append')
    
    # Создаем DataFrame с признаками комментариев, которые не меняются со временем
    
    comments_info_df = comments_df.drop(['current_time','score'], 
                                        axis = 1)
    
    # Получаем имена всех комментаторов
    
    comments_info_df['author_name'] = comments_info_df['author_name'].astype(str)
    
    # Оставляем только те записи, которых еще нет в таблице
    
    comments_new_df = comments_info_df[comments_info_df['created'] > before_comments_time]
    
    current_comments_time = before_comments_time.copy()
    
    if(not comments_new_df.empty):
        
        current_comments_time = comments_new_df['created'].max()
        
        # Записываем в таблицу информацию о новых постах
    
        comments_new_df.to_sql('comments_info',
                               conn,
                               index = False,
                               if_exists = 'append')
    
    return current_comments_time


# ### Запускаем скрипты

# In[96]:


s_time = int(time.time())

# Получаем посты и комментарии

try:
    
    posts_df = pd.DataFrame()
    comments_df = pd.DataFrame()

    for subreddit_one in subreddits:

        post_df, comment_df = get_data(subreddit_one, submission_limit, comments_depth)

        posts_df = pd.concat([posts_df, post_df], ignore_index = True)
        comments_df = pd.concat([comments_df, comment_df], ignore_index = True)

    # Сохраняем посты

    current_posts_time = save_posts(posts_df)

    # Сохраняем комментарии

    current_comments_time = save_comments(comments_df)
    
except Exception as e:
    
    print(e)
    
    bot.send_message(-545966419, 'Something went wrong: ' + str(e))


# In[97]:


time_posts_and_comments_df = pd.DataFrame([[current_time, 
                                            current_posts_time, 
                                            current_comments_time]],
                                            columns = time_posts_and_comments.columns)

time_posts_and_comments_df.to_sql('time_posts_and_comments',
                                  conn,
                                  index = False,
                                  if_exists = 'append')


# In[98]:


working_time = int(time.time()) - s_time

working_time = str(datetime.timedelta(seconds = working_time))


# In[99]:


bot.send_message(-545966419, 'Bot finished. Working time: ' + working_time)


# In[ ]:




